db.fruits.aggregate([
		{ $match: {onSale: true} },
		{ $count: "Fruits on sale: "}

	]);

db.fruits.aggregate([
		{ $match: {stock: {$gt: 20 }} },
		{ $count: "Fruits with stock more than 20: "}

	]);

db.fruits.aggregate([
		{ $match: {onSale: true} },
		{ $group: { _id: "$supplier_id", fruits_onSale_price: {$avg: "$price"} }}

]);


db.fruits.aggregate([
		{ $group: { _id: "$supplier_id", highest_price: {$max: "$price"} }}

	]);

db.fruits.aggregate([
		{ $group: { _id: "$supplier_id", lowest_price : {$min: "$price"} }}

	]);
